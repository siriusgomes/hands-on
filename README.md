Hands-on Ci&T. 

Interface para consumir API da Marvel. Busca por characters e listagem de seus respectivos comics. 
Cacheamento dos characters e comics visualizados utilizando HashMap. Disponibilizando os em formato JSON.

Para rodar: mvn spring-boot:run

Acessar: http://localhost:8080/

Dados consolidados (cacheados) disponíveis em:

http://localhost:8080/json/characters
http://localhost:8080/json/comics

Testes unitários: mvn test
