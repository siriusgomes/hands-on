package br.com.treinamento.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import br.com.treinamento.constant.Constants;
import br.com.treinamento.helper.*;
import br.com.treinamento.model.ApplicationDB;
import br.com.treinamento.pojo.CharacterAPIRequest;
import br.com.treinamento.pojo.Comics;
import br.com.treinamento.pojo.ComicsAPIRequest;

@Controller
public class MainController {

	@Autowired
	private APIConnector marvelApiConnector;

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/")
	public ModelAndView welcome() {
		return new ModelAndView("index");
	}

	
	/**
	 * ############################JSONs############################
	 */
	
	@RequestMapping(value = "/json/characters", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE )
    public ResponseEntity<String> getCharactersJson() {
        List<br.com.treinamento.pojo.Character> list = new ArrayList<br.com.treinamento.pojo.Character>( ApplicationDB.mapCharacters.values());

       
        String json = "";
        if (!list.isEmpty()) {
        	json = new Gson().toJson(list);
        }
        return new ResponseEntity<String>(json, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/json/comics", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE )
    public ResponseEntity<String> getComicsJson() {
		List<Comics> listComics = new ArrayList<Comics>();
		
		for (List<Comics> list : ApplicationDB.mapComics.values()) {
			listComics.addAll(list);
		}
       
        String json = "";
        if (!listComics.isEmpty()) {
        	json = new Gson().toJson(listComics);
        }
        return new ResponseEntity<String>(json, HttpStatus.OK);
    }

	
	
	/**
	 * ############################CHARACTERS############################
	 */

	@RequestMapping(value="/characters")
	public ModelAndView searchCharacters(@RequestParam("name") String name) {
		try {
			log.info("Buscando characters por com nome "+ name +" na API da Marvel.");
			String response = marvelApiConnector.doGet(Constants.CHARACTER_URL + "?nameStartsWith=" + name);
			ObjectMapper mapper = new ObjectMapper();
			CharacterAPIRequest c = mapper.readValue(response, CharacterAPIRequest.class);
			if (c.getCode() == 200 && c.getData().getTotal() > 0) {
				return new ModelAndView("characters", "characters", c.getData().getResults());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/character/{characId}")
	public ModelAndView getCharacter(@PathVariable Integer characId) {

		if (ApplicationDB.mapCharacters.containsKey(characId)) {
			log.info("Utilizando character bufferizado.");
			return new ModelAndView("character", "character", ApplicationDB.mapCharacters.get(characId));
		} else {
			try {
				log.info("Consultando characters na API da Marvel.");
				String response = marvelApiConnector.doGet(Constants.CHARACTER_URL + "/" + characId);
				ObjectMapper mapper = new ObjectMapper();
				CharacterAPIRequest c = mapper.readValue(response, CharacterAPIRequest.class);
				if (c.getCode() == 200 && c.getData().getTotal() > 0) {
					ApplicationDB.mapCharacters.put(c.getData().getResults().get(0).getId(),
							c.getData().getResults().get(0));
					return new ModelAndView("character", "character", ApplicationDB.mapCharacters.get(characId));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	@RequestMapping(value = "/character/{characId}/comics/{offset}")
	public ModelAndView getCharacterComics(@PathVariable Integer characId, @PathVariable Integer offset) {

		if (ApplicationDB.mapComics.containsKey(characId + offset)) {
			log.info("Utilizando comics bufferizado.");
			return new ModelAndView("comics", "comics", ApplicationDB.mapComics.get(characId + offset))
					.addObject("next", offset + 20).addObject("previous", offset - 20).addObject("characId", characId).addObject("comicsSize", ApplicationDB.mapComics.get(characId + offset).size());
		} else {
			try {
				log.info("Consultando comics na API da Marvel.");
				log.info(offset + "");
				String response = marvelApiConnector
						.doGet(Constants.CHARACTER_URL + "/" + characId + "/comics?offset=" + offset);
				ObjectMapper mapper = new ObjectMapper();
				ComicsAPIRequest c = mapper.readValue(response, ComicsAPIRequest.class);
				if (c.getCode() == 200 && c.getData().getTotal() > 0) {
					ApplicationDB.mapComics.put(characId + offset, c.getData().getResults());
					return new ModelAndView("comics", "comics", ApplicationDB.mapComics.get(characId + offset))
							.addObject("next", offset + 20).addObject("previous", offset - 20)
							.addObject("characId", characId).addObject("comicsSize", ApplicationDB.mapComics.get(characId + offset).size());
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

}
