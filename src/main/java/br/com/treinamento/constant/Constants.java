package br.com.treinamento.constant;

public class Constants {

	public static final String MARVEL_PUBLIC_KEY = "efe9ea42815e29faaf6c19dd9820d734";
	
	public static final String MARVEL_PRIVATE_KEY = "7dee7dc5ddb32166395d353f6977ec98b1f44a3a";
	
	public static final String IMAGE_TYPE = "portrait_fantastic"; // Valores Possíveis: portrait_small, portrait_medium, portrait_large, portrait_xlarge, portrait_fantastic, portrait_amazing
	
	public static final String CHARACTER_URL = "http://gateway.marvel.com/v1/public/characters";

}

