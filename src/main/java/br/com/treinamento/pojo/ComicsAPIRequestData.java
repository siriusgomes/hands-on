package br.com.treinamento.pojo;

import java.util.List;

import br.com.treinamento.pojo.defaults.APIRequestData;

public class ComicsAPIRequestData extends APIRequestData {

	private static final long serialVersionUID = -7772978700852971578L;
	
	private List<Comics> results;

	public List<Comics> getResults() {
		return results;
	}

	public void setResults(List<Comics> results) {
		this.results = results;
	}
	
}
