package br.com.treinamento.pojo;

import br.com.treinamento.pojo.defaults.APIRequest;

public class CharacterAPIRequest extends APIRequest {

	private static final long serialVersionUID = 735670190025384208L;
	
	private CharacterAPIRequestData data;

	public CharacterAPIRequestData getData() {
		return data;
	}

	public void setData(CharacterAPIRequestData data) {
		this.data = data;
	}
}
