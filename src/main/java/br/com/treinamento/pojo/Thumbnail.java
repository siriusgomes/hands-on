package br.com.treinamento.pojo;

import java.io.Serializable;

import br.com.treinamento.constant.Constants;

public class Thumbnail implements Serializable{

	@Override
	public String toString() {
		return "CharacterThumbnail [path=" + path + ", extension=" + extension + "]";
	}

	private static final long serialVersionUID = -2270564329626012330L;
	
	private String path;
	
	private String extension;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	/**
	 * Função para imprimir o path da imagem real
	 * Referência: https://developer.marvel.com/documentation/images 
	 * */
	public String getPathWithExtension() {
		return path + "/" + Constants.IMAGE_TYPE + "." + extension;
	}
}
