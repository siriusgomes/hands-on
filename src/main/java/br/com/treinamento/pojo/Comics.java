package br.com.treinamento.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Comics implements Serializable {

	@Override
	public String toString() {
		return "Comics [title=" + title + ", description=" + description + ", thumbnail=" + thumbnail + "]";
	}

	private static final long serialVersionUID = 8002721317341695166L;

	private String title;
	
	private String description;
	
	private Thumbnail thumbnail;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Thumbnail getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Thumbnail thumbnail) {
		this.thumbnail = thumbnail;
	}
	
}
