package br.com.treinamento.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Character implements Serializable {

	private static final long serialVersionUID = 5693331415965546273L;

	private Integer id;

	private String name;

	private String description;
	
	private Thumbnail thumbnail;
	
	private Quantity comics;
	
	private Quantity series;
	
	private Quantity stories;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Thumbnail getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Thumbnail thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	public Quantity getComics() {
		return comics;
	}

	public void setComics(Quantity comics) {
		this.comics = comics;
	}

	public Quantity getSeries() {
		return series;
	}

	public void setSeries(Quantity series) {
		this.series = series;
	}

	public Quantity getStories() {
		return stories;
	}

	public void setStories(Quantity stories) {
		this.stories = stories;
	}

	@Override
	public String toString() {
		return "Character [id=" + id + ", name=" + name + ", description=" + description + ", thumbnail=" + thumbnail
				+ ", comics=" + comics + ", series=" + series + ", stories=" + stories + "]";
	}

}
