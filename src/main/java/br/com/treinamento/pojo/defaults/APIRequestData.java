package br.com.treinamento.pojo.defaults;

import java.io.Serializable;

public class APIRequestData implements Serializable {
	
	@Override
	public String toString() {
		return "CharacterData [offset=" + offset + ", limit=" + limit + ", total=" + total + ", count=" + count
				+ "]";
	}

	private static final long serialVersionUID = -8821359963498346327L;

	private Integer offset;

	private Integer limit;

	private Integer total;

	private Integer count;

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
