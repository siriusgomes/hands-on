package br.com.treinamento.pojo.defaults;

import java.io.Serializable;

public class APIRequest implements Serializable {

	@Override
	public String toString() {
		return "APIRequest [code=" + code + ", status=" + status + ", copyright=" + copyright
				+ ", attributionText=" + attributionText + ", attributionHTML=" + attributionHTML + ", etag=" + etag
				+ "]";
	}

	private static final long serialVersionUID = 6769908113157080294L;

	private Integer code;
	
	private String status;
	
	private String copyright;
	
	private String attributionText;
	
	private String attributionHTML;
	
	private String etag;
	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public String getAttributionText() {
		return attributionText;
	}

	public void setAttributionText(String attributionText) {
		this.attributionText = attributionText;
	}

	public String getAttributionHTML() {
		return attributionHTML;
	}

	public void setAttributionHTML(String attributionHTML) {
		this.attributionHTML = attributionHTML;
	}

	public String getEtag() {
		return etag;
	}

	public void setEtag(String etag) {
		this.etag = etag;
	}
}
