package br.com.treinamento.pojo;

import java.util.List;

import br.com.treinamento.pojo.defaults.APIRequestData;

public class CharacterAPIRequestData extends APIRequestData {

	private static final long serialVersionUID = 3452952170032227188L;

	private List<Character> results;

	public List<Character> getResults() {
		return results;
	}

	public void setResults(List<Character> results) {
		this.results = results;
	}
	
}
