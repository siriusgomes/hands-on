package br.com.treinamento.pojo;

import br.com.treinamento.pojo.defaults.APIRequest;

public class ComicsAPIRequest extends APIRequest {

	private static final long serialVersionUID = -8331019621738501625L;
	
	private ComicsAPIRequestData data;

	public ComicsAPIRequestData getData() {
		return data;
	}

	public void setData(ComicsAPIRequestData data) {
		this.data = data;
	}
}
