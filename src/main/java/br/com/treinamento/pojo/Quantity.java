package br.com.treinamento.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
class Quantity implements Serializable {

	@Override
	public String toString() {
		return available + "";
	}

	private static final long serialVersionUID = -292614919723060160L;
	
	private Integer available;

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}
}