package br.com.treinamento.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.treinamento.pojo.Comics;

public class ApplicationDB {
	
	public static Map<Integer, br.com.treinamento.pojo.Character> mapCharacters = new HashMap<Integer, br.com.treinamento.pojo.Character>();
	
	public static Map<Integer, List<Comics>> mapComics = new HashMap<Integer, List<Comics>>();
	
}
