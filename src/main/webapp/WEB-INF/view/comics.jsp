<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>   

<h1>Showing Comics</h1>



<table border="2">
	<tr><th>Title</th><th>Description</th><th>Image</th></tr>
	<c:forEach var="comic" items="${comics}">
	<tr>
		<td>${comic.title}</td>
		<td>${comic.description}</td>
		<td><img src="${comic.thumbnail.pathWithExtension}"></td>
	</tr>
	</c:forEach>  
	<c:if test = "${previous >= 0}">
		<a href=/character/${characId}/comics/${previous}> <-Previous </a>
	</c:if>
	<c:if test = "${comicsSize == 20}">
		<a href=/character/${characId}/comics/${next}> Next -></a>
	</c:if>
</table>
