<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<h1>Showing character</h1>

<pre>${time}</pre>

<table>
	<tr>
		<td>ID:</td>
		<td>${character.id}</td>
	</tr>
	<tr>
		<td>Name:</td>
		<td>${character.name}</td>
	</tr>
	<tr>
		<td>Description:</td>
		<td>${character.description}</td>
	</tr>
	<tr>
		<td>Image:</td>
		<td><img src="${character.thumbnail.pathWithExtension}"></td>
	</tr>
	<tr>
		<td>Number of Comics:</td>
		<td><a href=/character/${character.id}/comics/0>${character.comics}</a></td>
	</tr>
</table>
