<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>   

<h1>Showing Comics</h1>



<table border="2">
	<tr><th>ID</th><th>Name</th><th>Description</th><th>Image</th></tr>
	<c:forEach var="character" items="${characters}">
	<tr>
		<td><a href="/character/${character.id}">${character.id}</a></td>
		<td>${character.name}</td>
		<td>${character.description}</td>
		<td><img src="${character.thumbnail.pathWithExtension}"></td>
	</tr>
	</c:forEach>
</table>
