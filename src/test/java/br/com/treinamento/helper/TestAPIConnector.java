package br.com.treinamento.helper;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import br.com.treinamento.constant.Constants;

@Component
public class TestAPIConnector {

	/**
	 * Função que faz um get na uri @param uri utilizando as chaves da API
	 * Marvel. A função faz a request passando um hash
	 */
	public String doGet(String uri) {
		String response = null;
		try {
			uri = uri.replace(" ", "%20");
			Long ts = System.currentTimeMillis();

			String hashString = ts + Constants.MARVEL_PRIVATE_KEY + Constants.MARVEL_PUBLIC_KEY;
			String hash = DigestUtils.md5DigestAsHex(hashString.getBytes()).toString();

			ClientConfig config = new ClientConfig();
			Client client = ClientBuilder.newClient(config);

			response = client.target(uri).queryParam("ts", ts).queryParam("apikey", Constants.MARVEL_PUBLIC_KEY).queryParam("hash", hash).request("application/json").get(String.class);

		} catch (Exception e) {
			System.err.println("Erro ao realizar request na API da Marvel:");
			e.printStackTrace();
		}
		return response;
	}

}
