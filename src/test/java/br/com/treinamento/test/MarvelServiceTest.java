package br.com.treinamento.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.treinamento.config.WebAppInitializer;
import br.com.treinamento.constant.Constants;
import br.com.treinamento.helper.APIConnector;
import br.com.treinamento.pojo.CharacterAPIRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@ComponentScan("br.com.treinamento.*")
@ContextConfiguration(classes = {WebAppInitializer.class})
public class MarvelServiceTest {

    private APIConnector apiConnector = new APIConnector();
    
    @Test
    public void testGetCaptainAmerica() throws Exception {
    	
    	String response = apiConnector.doGet(Constants.CHARACTER_URL + "?name=captain america");
    	ObjectMapper mapper = new ObjectMapper();
		CharacterAPIRequest c = mapper.readValue(response, CharacterAPIRequest.class);
       
        Assert.assertTrue(c.getCode() == 200);
        Assert.assertTrue(c.getData().getCount() > 0);
        Assert.assertTrue(c.getData().getResults().get(0).getId() == 1009220);
 
    }
    
}
